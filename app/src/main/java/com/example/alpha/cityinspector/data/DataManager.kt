package com.example.alpha.cityinspector.data

import android.content.Context
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.gson.Gson

class DataManager private constructor() {
    private object Holder {
        val INSTANCE = DataManager()
    }

    companion object {
        val instance: DataManager by lazy { Holder.INSTANCE }
    }

    var restaurants = emptyList<Restaurant>()
        private set
    var cuisines = emptyList<String>()
        private set
    var boroughs = emptyList<String>()
        private set
    var grades = emptyList<String>()
        private set

    fun initData(context: Context, successListener: () -> Unit, errorListener: () -> Unit) {
        val requestQueue = Volley.newRequestQueue(context)
        val request = StringRequest(
                "https://raw.githubusercontent.com/mongodb/docs-assets/primer-dataset/primer-dataset.json",
                Response.Listener {
                    handleResponse(it)
                    successListener()
                },
                Response.ErrorListener { errorListener() }
        )
        requestQueue.add(request)
    }

    private fun handleResponse(response: String) {
        val jsonObjects = response.split("\n")
        val gson = Gson()
        restaurants = jsonObjects.mapNotNull { jsonObject ->
            gson.fromJson(jsonObject, Restaurant::class.java)
        }
        cuisines = restaurants.map { it.cuisine }.distinct().sorted()
        boroughs = restaurants.map { it.borough }.distinct().sorted()
        grades = restaurants.mapNotNull { it.grades.firstOrNull()?.grade }.distinct().sorted()
    }
}
