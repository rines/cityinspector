package com.example.alpha.cityinspector.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.View
import com.example.alpha.cityinspector.R
import com.example.alpha.cityinspector.data.DataManager
import com.example.alpha.cityinspector.data.Restaurant
import com.example.alpha.cityinspector.viewmodel.FilterViewModel
import com.example.alpha.cityinspector.viewmodel.RestaurantViewModel
import kotlinx.android.synthetic.main.activity_main.*
import java.text.DecimalFormat


class MainActivity : AppCompatActivity(), FilterDialogFragment.FilterDialogFragmentCallback {
    private lateinit var filterViewModels: List<FilterViewModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        filterButton.setOnClickListener {
            FilterDialogFragment.newInstance(filterViewModels).show(supportFragmentManager, null)
        }

        initData()
    }

    private fun initData() {
        progressBar.visibility = View.VISIBLE
        DataManager.instance.initData(this, { handleDataSuccess() }, { handleDataError() })
    }

    private fun handleDataSuccess() {
        setupRecyclerView()
        setupFilters()
        setupSearch()
        displayData(DataManager.instance.restaurants)
        progressBar.visibility = View.GONE
    }

    private fun displayData(restaurants: List<Restaurant>) {
        val restaurantViewModels = restaurants.map { RestaurantViewModel(it) }
        recyclerView.adapter = RestaurantsAdapter(restaurantViewModels)
        showResultsSnackbar(restaurants)
    }

    private fun setupRecyclerView() {
        recyclerView.visibility = View.VISIBLE
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
    }

    private fun setupFilters() {
        filterButton.show()
        filterViewModels = listOf(
                FilterViewModel(R.string.cuisine, DataManager.instance.cuisines, { selection, restaurant ->
                    selection.contains(restaurant.cuisine)
                }),
                FilterViewModel(R.string.borough, DataManager.instance.boroughs, { selection, restaurant ->
                    selection.contains(restaurant.borough)
                }),
                FilterViewModel(R.string.grade, DataManager.instance.grades, { selection, restaurant ->
                    selection.contains(restaurant.grades.firstOrNull()?.grade)
                })
        )
    }

    private fun setupSearch() {
        searchView.visibility = View.VISIBLE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                searchView.clearFocus()
                applyFilters()
                return true
            }
        })
        searchView.setOnCloseListener {
            searchView.clearFocus()
            applyFilters()
            true
        }
    }

    private fun handleDataError() {
        progressBar.visibility = View.GONE
        AlertDialog.Builder(this)
                .setMessage(R.string.error_message)
                .setPositiveButton(R.string.retry) { dialogInterface, _ ->
                    dialogInterface.dismiss()
                    initData()
                }
                .setNegativeButton(R.string.exit) { _, _ ->
                    finish()
                }
                .setCancelable(false)
                .create()
                .show()
    }

    private fun applyFilters() {
        var restaurants = DataManager.instance.restaurants
        if (!searchView.query.isEmpty()) {
            restaurants = restaurants.filter { it.name.contains(searchView.query, true) }
        }
        for (filterViewModel in filterViewModels) {
            val selectionList = filterViewModel.selectionList()
            if (selectionList.any()) {
                restaurants = restaurants.filter { filterViewModel.filterPredicate(selectionList, it) }
            }
        }
        displayData(restaurants)
    }

    private fun showResultsSnackbar(restaurants: List<Restaurant>) {
        Snackbar.make(
                coordinatorLayout,
                resources.getQuantityString(
                        R.plurals.results_found,
                        restaurants.size,
                        DecimalFormat("#,###,###").format(restaurants.size)
                ),
                Snackbar.LENGTH_LONG
        ).show()
    }

    override fun onFilterDialogComplete(filterViewModels: List<FilterViewModel>) {
        this.filterViewModels = filterViewModels
        applyFilters()
    }
}
