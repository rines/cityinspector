package com.example.alpha.cityinspector.viewmodel

import com.example.alpha.cityinspector.data.Restaurant
import java.text.SimpleDateFormat
import java.util.*


class RestaurantViewModel(restaurant: Restaurant) {
    companion object {
        private val gradeDateFormatter = SimpleDateFormat("MMM yyyy", Locale.getDefault())
    }

    private val latestGrade = restaurant.grades.firstOrNull()

    val name = restaurant.name.trim()
    val cuisine = restaurant.cuisine
    val address = "${restaurant.address.building.trim()} ${restaurant.address.street.trim()}\n${restaurant.borough}, NY ${restaurant.address.zipcode}"
    val hasGrade = latestGrade != null
    val grade = if (latestGrade?.grade?.length == 1) latestGrade.grade else "?"
    val gradeDate = latestGrade?.date?.get()?.let { gradeDateFormatter.format(it) }
}
