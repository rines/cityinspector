package com.example.alpha.cityinspector.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.alpha.cityinspector.R
import com.example.alpha.cityinspector.viewmodel.RestaurantViewModel
import kotlinx.android.synthetic.main.restaurant_list_item.view.*

class RestaurantsAdapter(private val restaurantViewModels: List<RestaurantViewModel>) : RecyclerView.Adapter<RestaurantsAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(restaurantViewModels[position])
    }

    override fun getItemCount(): Int {
        return restaurantViewModels.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.restaurant_list_item, parent, false)
        return ViewHolder(itemView)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(viewModel: RestaurantViewModel) {
            itemView.name.text = viewModel.name
            itemView.cuisine.text = viewModel.cuisine
            itemView.address.text = viewModel.address
            if (viewModel.hasGrade) {
                itemView.gradeSection.visibility = View.VISIBLE
                itemView.grade.text = viewModel.grade
                itemView.gradeDate.text = viewModel.gradeDate
            } else {
                itemView.gradeSection.visibility = View.INVISIBLE
            }
        }
    }
}