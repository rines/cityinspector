package com.example.alpha.cityinspector.viewmodel

import com.example.alpha.cityinspector.data.Restaurant
import java.io.Serializable

class FilterViewModel(
        val titleId: Int,
        val choices: List<String>,
        val filterPredicate: (List<String>, Restaurant) -> Boolean
) : Serializable {
    val selectionFlags = BooleanArray(choices.size)

    fun selectionText(): String {
        return if (selectionFlags.any { it }) {
            selectionList().joinToString(", ") { "\"$it\"" }
        } else {
            "All"
        }
    }

    fun selectionList(): List<String> {
        return selectionFlags.mapIndexed { index, flag -> if (flag) choices[index] else null }
                .filterNotNull()
    }
}
