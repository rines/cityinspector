package com.example.alpha.cityinspector.ui

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v7.app.AlertDialog
import android.view.View
import com.example.alpha.cityinspector.R
import com.example.alpha.cityinspector.viewmodel.FilterViewModel
import kotlinx.android.synthetic.main.dialog_fragment_filter.view.*
import kotlinx.android.synthetic.main.view_filter.view.*


class FilterDialogFragment : BottomSheetDialogFragment() {
    interface FilterDialogFragmentCallback {
        fun onFilterDialogComplete(filterViewModels: List<FilterViewModel>)
    }

    companion object {
        private val VIEW_MODELS = "view_models"

        fun newInstance(filterViewModels: List<FilterViewModel>): FilterDialogFragment {
            val dialogFragment = FilterDialogFragment()
            val arguments = Bundle()
            arguments.putSerializable(VIEW_MODELS, filterViewModels.toTypedArray())
            dialogFragment.arguments = arguments
            return dialogFragment
        }
    }

    private lateinit var filterViewModels: List<FilterViewModel>

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog, style: Int) {
        super.setupDialog(dialog, style)
        val contentView = View.inflate(context, R.layout.dialog_fragment_filter, null)
        filterViewModels = (arguments.getSerializable(VIEW_MODELS) as Array<FilterViewModel>).toList()
        for (filterViewModel in filterViewModels) {
            val filterView = View.inflate(context, R.layout.view_filter, null)
            filterView.title.text = context.getText(filterViewModel.titleId)
            filterView.selection.text = filterViewModel.selectionText()
            filterView.setOnClickListener { showFilterDialog(filterView, filterViewModel) }
            contentView.filters.addView(filterView)
        }
        dialog.setContentView(contentView)
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        val potentialCallback = activity
        if (potentialCallback is FilterDialogFragmentCallback) {
            potentialCallback.onFilterDialogComplete(filterViewModels)
        }
    }

    private fun showFilterDialog(filterView: View, viewModel: FilterViewModel) {
        AlertDialog.Builder(context)
                .setTitle(viewModel.titleId)
                .setMultiChoiceItems(
                        viewModel.choices.toTypedArray(),
                        viewModel.selectionFlags,
                        { _, choiceIndex, flag ->
                            viewModel.selectionFlags[choiceIndex] = flag
                        }
                )
                .setPositiveButton(R.string.done, { _, _ ->
                    filterView.selection.text = viewModel.selectionText()
                })
                .create()
                .show()
    }
}
