package com.example.alpha.cityinspector.data

import com.google.gson.annotations.SerializedName
import java.util.*


data class Restaurant(
        @SerializedName("restaurant_id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("address") val address: Address,
        @SerializedName("borough") val borough: String,
        @SerializedName("cuisine") val cuisine: String,
        @SerializedName("grades") val grades: List<Grade>
)

data class Address(
        @SerializedName("building") val building: String,
        @SerializedName("coord") private val coordinates: List<Double>,
        @SerializedName("street") val street: String,
        @SerializedName("zipcode") val zipcode: String
) {
    val latLng: LatLng? = if (coordinates.size == 2) {
        LatLng(coordinates[0], coordinates[1])
    } else {
        null
    }
}

data class LatLng(
        val lat: Double,
        val lng: Double
)

data class Grade(
        @SerializedName("date") val date: DateWrapper,
        @SerializedName("grade") val grade: String,
        @SerializedName("score") val score: Int
)

data class DateWrapper(
        @SerializedName("\$date") private val date: Long
) {
    fun get(): Date {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = date
        return calendar.time
    }
}